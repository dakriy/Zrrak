#pragma once
// Gloabl includes
#include <string>
#include <vector>
#include <SDL.h>
#include "Render.h"
#include "ErrorHandler.h"
#include "SDL_ttf.h"
#include "SDL_image.h"
#include "Text.h"
#include "Rectangle.h"
#include "Map.h"
#include <dirent.h>

#ifndef INCLUDES_HEADERS
#define INCLUDES_HEADERS

	#define SCREENX 1280
	#define SCREENY 720
	#define LOGFILE "log.txt"

	extern Render * Screen;
	extern Map * Design;
	extern std::vector<string> TileNames;
	extern std::vector<string> EntityNames;
	extern void logTTFError(string currentFile);
	extern void logSDLError(string currentFile);
	extern void logError(string type, string currentFile, string error);
	extern void getTiles();
	extern void getEntities();
#endif