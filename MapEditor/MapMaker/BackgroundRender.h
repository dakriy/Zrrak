#pragma once

#include "Globals.h"
#include "SDL.h"
#include "Render.h"

class BackgroundRender
{
protected:
	SDL_Texture * Bkg_texture = nullptr;
	int pheight, pwidth;
	SDL_Rect * pictureSelection;
	SDL_Rect * picturePosition;
	int * bkgIndex;
	Rectangle * Artificial_Bkg;
	void readyBackground();
	bool Artificial;
	SDL_Color * RectColor;
	std::vector<Item *> Tiles;
	std::vector<Entity *> Entities;
public:
	BackgroundRender();
	void loadBackground(bool artifical = true, char* IMG_PATH = NULL);
	void addTiles();
	void addEntities();
	~BackgroundRender();
};