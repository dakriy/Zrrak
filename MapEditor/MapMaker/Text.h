#pragma once
#include "Globals.h"

class Text
{
protected:
	string Message;
	SDL_Color Color;
	int fontSize;
	TTF_Font * font;
	string fontType;
	SDL_Texture * textTexture;
	int * textureIndex;
	SDL_Rect * Placement;
	SDL_Rect * Size;
public:
	Text();
	void setColor(SDL_Color color);
	void setFontSize(int fontSize);
	void setMessage(string message);
	void setFontType(string file);
	// Passing false to this will use default size values EG map it to the font size
	void Render(bool renderNoSize = true);
	void setSize(int x, int y, int w, int h);
	void setSize(SDL_Rect * size);
	void setPlacement(int x, int y, int w, int h);
	void setPlacement(SDL_Rect * placement);
	~Text();
};