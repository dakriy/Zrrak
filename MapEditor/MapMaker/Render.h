#pragma once

#include "Globals.h"
#include "SDL.h"
#include <SDL_image.h>
#include <vector>
#include "Rectangle.h"
#include "Map.h"

using namespace std;

// Any pointers passed will be destroyed
class Render
{
protected:
	SDL_Renderer * Renderer;
	vector<SDL_Texture *> Textures;
	vector<SDL_Rect *> TextureRectPlace;
	vector<SDL_Rect *> TextureRectSize;
	vector<int *> activeTextures;
	vector<Rectangle *> Rectangles;
public:
	Render(SDL_Window * window);
	void display();
	void draw();
	void clearScreen();
	void present();
	// returns locaton in the vector
	int * addTexture(SDL_Texture * texture_to_add, SDL_Rect * TextureSelection = nullptr, SDL_Rect * TexturePlacement = nullptr);
	void removeTexture(int *index);
	int * insertTexture(int *index, SDL_Texture * texture_to_add, SDL_Rect * TextureSelection = nullptr, SDL_Rect * TexturePlacement = nullptr);
	int * replaceTexture(int *index, SDL_Texture * texture_to_replace, SDL_Rect * TextureSelection = nullptr, SDL_Rect * TexturePlacement = nullptr);
	SDL_Rect * getTextureRectPlace(int *index);
	SDL_Rect * getTextureRectSize(int * index);
	SDL_Renderer * getRenderer();
	int * addRectangle(Rectangle * rect);
	void removeRectangle(int * index);
	void remove_without_delete(int * index);
	~Render();
};