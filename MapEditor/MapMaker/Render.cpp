#include "Globals.h"
#include "SDL.h"
#include <SDL_image.h>
#include "Render.h"
#include "BackgroundRender.h"
#include <iostream>

using namespace std;

Render::Render(SDL_Window* window)
{
	this->Renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
}

void Render::clearScreen()
{
	SDL_RenderClear(this->Renderer);
}

void Render::present()
{
	this->clearScreen();
	this->draw();
	this->display();
}

void Render::draw()
{
	if (Design != nullptr)
	{
		// puts current map tiles on the coorect position on the screen.
		Design->renderMap();
	}
	for (int i = 0; i < Rectangles.size(); i++)
	{
		Rectangles[i]->draw();
	}
	for (int i = 0; i < Textures.size(); i++)
	{
		SDL_RenderCopy(this->Renderer, this->Textures[i], this->TextureRectSize[i], this->TextureRectPlace[i]);
	}
}

void Render::display()
{
	SDL_RenderPresent(this->Renderer);
}

int * Render::addTexture(SDL_Texture * texture_to_add, SDL_Rect * TextureSelection, SDL_Rect * TexturePlacement)
{
	this->TextureRectPlace.push_back(TexturePlacement);
	this->TextureRectSize.push_back(TextureSelection);
	this->Textures.push_back(texture_to_add);
	int * index = new int;
	*index = Textures.size();
	// Put it on the active textures array for later revising.
	activeTextures.push_back(index);
	*index -= 1;
	return index;
}

// Deletes the index int pointer as well as the texture
void Render::removeTexture(int *index)
{
	if (*index < Textures.size())
	{
		// Erase texture
		SDL_DestroyTexture(this->Textures[*index]);
		// Erase texture reference
		this->Textures.erase(Textures.begin()+*index);
		// Where values index values have to be subtracted from
		int later = -1;
		//
		for (int i = 0; i < activeTextures.size(); i++)
		{
			if (*activeTextures[i] == *index)
			{
				// Save value for easy changing of valuse later
				later = i;
				// Dereference for safety and so user doesn't try to reference it later
				delete activeTextures[i];
				// Erase Texture from vector
				delete TextureRectPlace[i];
				delete TextureRectSize[i];
				activeTextures.erase(activeTextures.begin() + i);
				TextureRectPlace.erase(TextureRectPlace.begin() + i);
				TextureRectSize.erase(TextureRectSize.begin() + i);
				break;
			}
		}
		// Redefine all user references
		if (later != -1)
		{
			for (int i = later; i < activeTextures.size(); i++)
			{
				*activeTextures[i] -= 1;
			}
		}
	}
}

int * Render::insertTexture(int *index, SDL_Texture* texture_to_add, SDL_Rect* TextureSelection, SDL_Rect* TexturePlacement)
{
	if(*index >= 0 && *index < Textures.size())
	{
		for (auto i = *index; i < activeTextures.size(); i++)
		{
			// Change values of the referenced indexes
			*activeTextures[i] += 1;
		}
		int * newIndex = new int;
		newIndex = index;
		// Add texture
		this->activeTextures.insert(activeTextures.begin() + *index, newIndex);
		this->Textures.insert(this->Textures.begin() + *index, texture_to_add);
		this->TextureRectPlace.insert(TextureRectPlace.begin() + *index, TexturePlacement);
		this->TextureRectSize.insert(TextureRectSize.begin() + *index, TextureSelection);
		return activeTextures[*index];
	}
	return nullptr;
}

int * Render::replaceTexture(int * index, SDL_Texture* texture_to_replace, SDL_Rect* TextureSelection, SDL_Rect* TexturePlacement)
{
	if (*index >= 0 && *index < Textures.size())
	{
		SDL_DestroyTexture(this->Textures[*index]);
		delete TextureRectPlace[*index];
		delete TextureRectSize[*index];
		this->TextureRectPlace[*index] = TexturePlacement;
		this->TextureRectSize[*index] = TextureSelection;
		this->Textures[*index] = texture_to_replace;
		return index;
	}
	return nullptr;
}

SDL_Rect* Render::getTextureRectPlace(int* index)
{
	return TextureRectPlace[*index];
}

SDL_Rect* Render::getTextureRectSize(int* index)
{
	return TextureRectSize[*index];
}

SDL_Renderer* Render::getRenderer()
{
	return this->Renderer;
}

// User has to delete index pointer if this is manually used.
int* Render::addRectangle(Rectangle* rect)
{
	this->Rectangles.push_back(rect);
	int * index = new int;
	*index = Rectangles.size() - 1;
	return index;
}

void Render::removeRectangle(int* index)
{
	delete this->Rectangles[*index];
	this->Rectangles[*index] = nullptr;
}

void Render::remove_without_delete(int* index)
{
	if (*index < Textures.size())
	{
		// Erase texture reference
		this->Textures.erase(Textures.begin() + *index);
		// Where values index values have to be subtracted from
		int later = -1;
		//
		for (int i = 0; i < activeTextures.size(); i++)
		{
			if (*activeTextures[i] == *index)
			{
				// Save value for easy changing of valuse later
				later = i;

				activeTextures.erase(activeTextures.begin() + i);
				TextureRectPlace.erase(TextureRectPlace.begin() + i);
				TextureRectSize.erase(TextureRectSize.begin() + i);
				break;
			}
		}
		// Redefine all user references
		if (later != -1)
		{
			for (int i = later; i < activeTextures.size(); i++)
			{
				*activeTextures[i] -= 1;
			}
		}
	}
}

Render::~Render()
{
	for (auto i = 0; i < Textures.size(); i++)
	{
		SDL_DestroyTexture(Textures[i]);
		delete TextureRectPlace[i];
		delete TextureRectSize[i];
		Textures[i] = nullptr;
	}
	for (auto i = 0; i < activeTextures.size(); i++)
	{
		delete activeTextures[i];
		TextureRectPlace[i] = nullptr;
		TextureRectSize[i] = nullptr;
	}
	for (auto i = 0; i < Rectangles.size(); i++)
	{
		delete Rectangles[i];
	}
	this->Rectangles.clear();
	this->Textures.clear();
	this->TextureRectPlace.clear();
	this->TextureRectSize.clear();
	this->activeTextures.clear();
	SDL_DestroyRenderer(this->getRenderer());
}
