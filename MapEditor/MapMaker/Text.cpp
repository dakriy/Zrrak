#include "Text.h"
#include "Globals.h"

Text::Text()
{
	this->Placement = nullptr;
	this->Size = nullptr;
	this->textTexture = nullptr;
	this->fontSize = 28;
	this->Color = { 255, 255, 255 };
	this->fontType = "C:\\Windows\\Fonts\\arial.ttf";
	this->font = TTF_OpenFont(fontType.c_str(), fontSize);
	if (font == nullptr) {
		logTTFError("main.cpp");
	}
	this->textureIndex = nullptr;
}

void Text::setColor(SDL_Color color)
{
	this->Color = color;
}

void Text::setFontSize(int size)
{
	this->fontSize = size;
}

void Text::setMessage(string message)
{
	this->Message = message;
}

void Text::setFontType(string file)
{
	this->fontType = file;
	TTF_CloseFont(this->font);
	this->font = TTF_OpenFont(fontType.c_str(), fontSize);
	if (font == nullptr) 
	{
		logTTFError("main.cpp");
	}
}

void Text::Render(bool renderNoSize)
{
	SDL_Surface * text_surface = TTF_RenderText_Solid(this->font, this->Message.c_str(), this->Color);
	if (text_surface == nullptr) {
		logTTFError("main.cpp");
	}
	SDL_Texture *texture = SDL_CreateTextureFromSurface(Screen->getRenderer(), text_surface);
	if (texture == nullptr) {
		logSDLError("main.cpp");
	}
	SDL_FreeSurface(text_surface);

	if(this->textureIndex == nullptr)
	{
		this->textureIndex = Screen->addTexture(texture, (renderNoSize) ? NULL : this->Size, this->Placement);
	} else
	{
		Screen->replaceTexture(this->textureIndex, texture, (renderNoSize) ? NULL : this->Size, this->Placement);
	}
}

void Text::setSize(int x, int y, int w, int h)
{
	this->Size->x = x;
	this->Size->y = y;
	this->Size->w = w;
	this->Size->h = h;
}

void Text::setPlacement(int x, int y, int w, int h)
{
	this->Placement->x = x;
	this->Placement->y = y;
	this->Placement->w = w;
	this->Placement->h = h;
}

void Text::setPlacement(SDL_Rect* placement)
{
	this->Placement = placement;
}

Text::~Text()
{
	TTF_CloseFont(this->font);
	Screen->removeTexture(this->textureIndex);
}

void Text::setSize(SDL_Rect* size)
{
	this->Size = size;
}