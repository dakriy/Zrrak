#pragma once
#include "Globals.h"

class Rectangle
{
protected:
	SDL_Texture * Texture;
	SDL_Rect * Size_and_Position;
	SDL_Color * Color;
	int * RenderingIndex;
public:
	Rectangle(SDL_Rect * size_and_position, SDL_Color * color);
	void draw();
	void destory();
	void editRectangle(SDL_Rect * size_and_position, SDL_Color * color);
	void editRectangle(SDL_Rect * size_and_position);
	void editRectangle(SDL_Color * color);
	~Rectangle();
};