#include "BackgroundRender.h"
#include "Globals.h"
#include "SDL.h"
#include <SDL_image.h>

BackgroundRender::BackgroundRender()
{
	this->bkgIndex = nullptr;
	this->Artificial_Bkg = nullptr;
	this->RectColor = nullptr;
}

void BackgroundRender::loadBackground(bool artifical, char* IMG_PATH)
{
	this->Artificial = artifical;
	if (!this->Artificial)
	{
		this->pictureSelection = new SDL_Rect;
		this->picturePosition = new SDL_Rect;
		// Load the picture that is to be the background
		Bkg_texture = IMG_LoadTexture(Screen->getRenderer(), IMG_PATH);

		// Get background picture properties
		SDL_QueryTexture(this->Bkg_texture, NULL, NULL, &this->pwidth, &this->pheight);

		// Select the whole picture
		this->pictureSelection->w = pwidth;
		this->pictureSelection->h = pheight;
		this->pictureSelection->x = 0;
		this->pictureSelection->y = 0;

		// Set the background to be the size of the screen
		this->picturePosition->x = 0;
		this->picturePosition->y = 0;
		this->picturePosition->w = SCREENX;
		this->picturePosition->h = SCREENY;
	} else
	{
		this->pictureSelection = nullptr;
		this->picturePosition = new SDL_Rect;
		this->picturePosition->x = SCREENX - (SCREENX / 4);
		this->picturePosition->y = 0;
		this->picturePosition->w = SCREENX / 4;
		this->picturePosition->h = SCREENY;
	}

	// Load it into the renderer to be rendered
	this->readyBackground();
}

void BackgroundRender::addTiles()
{
	for (int i = 0; i < TileNames.size(); i++)
	{
		Item * item = new Item;
		SDL_Rect * position = new SDL_Rect;
		string name = "Tiles/" + TileNames[i];
		item->setTexture(IMG_LoadTexture(Screen->getRenderer(), name.c_str()));

		position->x = this->picturePosition->x + 32 * i;
		position->y = 0;
		position->w = 32;
		position->h = 32;
		item->setPlacement(position);
	}
}

void BackgroundRender::addEntities()
{
	for (int i = 0; i < EntityNames.size(); i++)
	{
		
	}
}

void BackgroundRender::readyBackground()
{
	if (this->Artificial)
	{
		this->RectColor = new SDL_Color;
		this->RectColor->a = 0;
		this->RectColor->r = 178;
		this->RectColor->b = 223;
		this->RectColor->g = 238;
		this->Artificial_Bkg = new Rectangle(this->picturePosition, this->RectColor);
	}
	else
	{
		// Add background to the screen
		this->bkgIndex = Screen->addTexture(this->Bkg_texture, this->pictureSelection, this->picturePosition);
	}
}

BackgroundRender::~BackgroundRender()
{
	if (this->Artificial)
	{
		Artificial_Bkg->destory();
	} else
	{
		// Delete the background... Renderer takes care of the allocated rectangles and index
		Screen->removeTexture(bkgIndex);
		this->bkgIndex = nullptr;
	}
}
