#pragma once

#include "Item.h"
#include "Entity.h"
#include "Globals.h"

struct SDL_Rect;

class Map
{
protected:
	std::string name;
	SDL_Rect * CurrentCoords;
	std::vector<Item *> Tiles;
	std::vector<Entity *> Entities;
public:
	Map();
	void addItem(Item * item_to_add);
	void removeItem(Item* item);
	void renderMap();
	void replaceItem(Item* item, Item* new_item);
	void addEntity(Entity * entity_to_add);
	void removeEntity(Entity * entity);
	void setRenderCoords(int x, int y);
	void saveMap();
	~Map();
};
