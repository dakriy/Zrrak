﻿#include "Item.h"

Item::Item(SDL_Texture* texture, SDL_Rect* position)
{
	this->setTexture(texture);
	this->setPlacement(position);
	this->Selection = new SDL_Rect;
	this->Selection->x = 0;
	this->Selection->y = 0;
	this->Selection->h = 32;
	this->Selection->w = 32;
}

SDL_Rect* Item::getPlacement()
{
	return this->Place;
}

void Item::setPlacement(SDL_Rect* position)
{
	this->Place = position;
}

void Item::setRenderingIndex(int* index)
{
	this->renderingIndex = index;
}

int* Item::getRenderingIndex()
{
	return this->renderingIndex;
}

void Item::setTexture(SDL_Texture* texture)
{
	this->Texture = texture;
}

SDL_Texture* Item::getTexture()
{
	return this->Texture;
}

SDL_Rect* Item::getSelection()
{
	return this->Selection;
}

Item::~Item()
{
	if (this->getTexture() != nullptr)
	{
		Screen->removeTexture(this->getRenderingIndex());
	}
	if (this->Selection != nullptr)
	{
		delete this->Selection;
	}
	if (this->Place != nullptr)
	{
		delete this->Place;
	}
}
