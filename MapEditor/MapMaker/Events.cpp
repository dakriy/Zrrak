#include "Events.h"
#include "Globals.h"
#include <iostream>

using namespace std;

void Events::updateMouseCoordinates()
{
	this->update();
	SDL_GetMouseState(&mousex, &mousey);
	mousex = (((float)mousex) - SCREENX / 2);
	mousey = -(((float)mousey) - SCREENY / 2);
}

bool Events::handleInput()
{
	this->update();
	if (SDL_PollEvent(&Event)) {
		if (Event.type == SDL_QUIT)
			return true;
		if (Event.type == SDL_KEYUP && Event.key.keysym.sym == SDLK_ESCAPE)
			return true;
			
	}
	return false;
}

int Events::getMousex()
{
	this->updateMouseCoordinates();
	return this->mousex;
}

int Events::getMousey()
{
	this->updateMouseCoordinates();
	return this->mousey;
}

void Events::update()
{
	SDL_PumpEvents();
}
