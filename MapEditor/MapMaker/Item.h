﻿#pragma once

#include "Globals.h"

class Item
{
public:
	// Do not bother deleting anything that gets passed to the constructor as it will be autmoatically deleted later
	Item(SDL_Texture * texture = nullptr, SDL_Rect * position = nullptr);
	SDL_Rect * getPlacement();
	void setPlacement(SDL_Rect * position);
	void setRenderingIndex(int * index);
	int * getRenderingIndex();
	void setTexture(SDL_Texture * texture);
	SDL_Texture * getTexture();
	SDL_Rect * getSelection();
	~Item();
protected:
	SDL_Rect * Place;
	SDL_Rect * Selection;
	int * renderingIndex;
	SDL_Texture * Texture;

};