#include "Globals.h"
#include <iostream>
#include "Events.h"
#include "BackgroundRender.h"

using namespace std;

void quit()
{
	TTF_Quit();
	SDL_Quit();
}

void initilization()
{
	TTF_Init();
	if (!TTF_WasInit())
	{
		logTTFError("main.cpp Initialization");
		quit();
		exit(EXIT_FAILURE | SDL_INIT_AUDIO);
	}
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_VIDEO) != 0)
	{
		logSDLError("main.cpp Initialization");
		quit();
		exit(EXIT_FAILURE);
	}
	getTiles();
	getEntities();
}

int main(int argc, char* argv[])
{
	initilization();
	SDL_Window * window;
	SDL_Init(SDL_INIT_VIDEO);

	window = SDL_CreateWindow(
		"Zrrak Map Maker",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		SCREENX,
		SCREENY,
		SDL_WINDOW_OPENGL
	);
	if (window == NULL || window == nullptr)
	{
		logSDLError("main.cpp");
		return 1;
	}
	
	Screen = new Render(window);
	BackgroundRender * bkg = new BackgroundRender();
	bkg->loadBackground(true, NULL);
	Design = new Map();
	unsigned int time_since_last_frame = 0;
	unsigned long long int frame = 0;
	unsigned long long int frames_passed = 0;
	Events * e = new Events;
	int * place = nullptr;
	while (!e->handleInput())
	{
		// game loop
		if (time_since_last_frame > 15.5)
		{
			frames_passed++;
			frame = SDL_GetTicks();

			Screen->present();
		}
		time_since_last_frame = SDL_GetTicks() - frame;
	}
	// delete created Pointers
	delete e;
	delete Design;
	delete bkg;
	delete Screen;
	SDL_DestroyWindow(window);
	quit();
	return 0;
}