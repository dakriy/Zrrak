#include "Globals.h"

#ifndef SCREEN_RENDERER
#define SCREEN_RENDERER
	Render * Screen = nullptr;
	Map * Design = nullptr;
	std::vector<string> TileNames;
	std::vector<string> EntityNames;
#endif
void logTTFError(string currentFile)
{
	ErrorHandler * Error = new ErrorHandler(LOGFILE);
	Error->LogError("TTF ERROR", TTF_GetError(), currentFile);
	delete Error;
}

void logSDLError(string currentFile)
{
	ErrorHandler * Error = new ErrorHandler(LOGFILE);
	Error->LogError("SDL ERROR", SDL_GetError(), currentFile);
	delete Error;
}

void logError(string type, string currentFile, string error)
{
	ErrorHandler * Error = new ErrorHandler(LOGFILE);
	Error->LogError(type, error, currentFile);
	delete Error;
}

void getTiles()
{
	DIR * dir;
	struct dirent* ent;
	if((dir = opendir("Tiles")) != NULL)
	{
		while((ent = readdir(dir)) != NULL)
		{
			TileNames.push_back(ent->d_name);
		}
		closedir(dir);
	}
	else
	{
		logError("File I/O", "Globals.cpp getTiles", "Could not open DIR");
	}
	delete ent;
	delete dir;
}

void getEntities()
{
	DIR * dir;
	struct dirent* ent;
	if ((dir = opendir("Entities")) != NULL)
	{
		while ((ent = readdir(dir)) != NULL)
		{
			EntityNames.push_back(ent->d_name);
		}
		closedir(dir);
	}
	else
	{
		logError("File I/O", "Globals.cpp getEntities", "Could not open DIR");
	}
	delete ent;
	delete dir;
}
