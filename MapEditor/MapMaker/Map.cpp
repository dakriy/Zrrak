﻿#include "Globals.h"
#include "Map.h"

Map::Map()
{
	this->CurrentCoords = new SDL_Rect;
	this->CurrentCoords->h = SCREENY;
	this->CurrentCoords->w = ((SCREENX / 4) * 3);
	this->CurrentCoords->x = 0;
	this->CurrentCoords->y = 0;
}

void Map::addItem(Item* item_to_add)
{
	Tiles.push_back(item_to_add);
}

void Map::removeItem(Item* item)
{
	// Go through to find the one to be removed
	for (int i = 0; i < Tiles.size(); i++)
	{
		if (item == Tiles[i])
		{
			delete Tiles[i];
			Tiles[i] = nullptr;
			Tiles.erase(Tiles.begin() + i);
			break;
		}
	}
}

// determines what parts of the map get rendered and adds the proper tiles to the renderer
void Map::renderMap()
{
	// This function is going to suck.
	// I MUST COMMENT THIS OTHERWISE I WILL SPED AN HOUR MORE THAN I NEEDED TO
	// I am listening to Thomas Bergersen - Colors of Love (Sun)  on youtube and I am pretty hyped. I know that sounds gay but shut up its a good song. Look it up
	// OK... FOCUS >.>
	//
	//
	// So how this works. (I need to write this down so I don't forget)
	// first it finds all the textures that are loaded by looking at the placment
	// variable in all the items and unloading them if it exists.
	// then it determines which ones to load based on coordinates in the item
	// and loads those into the renderer to be rendered

	int allowed_x_u = this->CurrentCoords->x + SCREENX;
	int allowed_x_l = this->CurrentCoords->x - SCREENX;
	int allowed_y_u = this->CurrentCoords->y + SCREENY;
	int allowed_y_l = this->CurrentCoords->y - SCREENY;

	for (int i = 0; i < Tiles.size(); i++)
	{
		SDL_Rect TilePlacement = *Tiles[i]->getPlacement();
		if (Tiles[i]->getRenderingIndex() != nullptr)
		{
			Screen->remove_without_delete(Tiles[i]->getRenderingIndex());
			Entities[i]->setRenderingIndex(nullptr);
		}
		if (((TilePlacement.x > allowed_x_l) && (TilePlacement.x < allowed_x_u)) && ((TilePlacement.y > allowed_y_l) && (TilePlacement.y < allowed_y_u)))
		{
			Tiles[i]->setRenderingIndex(Screen->addTexture(Tiles[i]->getTexture(), nullptr, Tiles[i]->getPlacement()));
		}
	}
	for (int i = 0; i < Entities.size(); i++)
	{
		SDL_Rect TilePlacement = *Entities[i]->getPlacement();
		if (Entities[i]->getRenderingIndex() != nullptr)
		{
			Screen->remove_without_delete(Entities[i]->getRenderingIndex());
			Entities[i]->setRenderingIndex(nullptr);
		}
		if (((TilePlacement.x > allowed_x_l) && (TilePlacement.x < allowed_x_u)) && ((TilePlacement.y > allowed_y_l) && (TilePlacement.y < allowed_y_u)))
		{
			Entities[i]->setRenderingIndex(Screen->addTexture(Entities[i]->getTexture(), nullptr, Entities[i]->getPlacement()));
		}
	}
	// That wasn't so bad.
}

void Map::replaceItem(Item* item, Item* new_item)
{
	this->removeItem(item);
	this->addItem(new_item);
}

void Map::addEntity(Entity* entity_to_add)
{
	Entities.push_back(entity_to_add);
}

void Map::removeEntity(Entity* entity)
{
	// Go through to find the one to be removed
	for (int i = 0; i < Entities.size(); i++)
	{
		if (entity == Entities[i])
		{
			delete Entities[i];
			// Yes this was copied and pasted.
			Entities[i] = nullptr;
			Entities.erase(Entities.begin() + i);
			break;
		}
	}
}

void Map::setRenderCoords(int x, int y)
{
	this->CurrentCoords->x = x;
	this->CurrentCoords->y = y;
}

// Saves the map to a file
void Map::saveMap()
{
	this->name = "MapYouJustMadePleaseRenameMe.dat";
	// This function is also going to suck.
}

Map::~Map()
{
	delete CurrentCoords;
	for (int i = 0; i < Tiles.size(); i++)
	{
		if (Tiles[i] != nullptr)
		{
			delete Tiles[i];
		}
	}

	for (int i = 0; i < Entities.size(); i++)
	{
		if(Entities[i] != nullptr)
		{
			delete Entities[i];
		}
	}
}
