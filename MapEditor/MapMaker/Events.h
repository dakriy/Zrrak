#pragma once
#include "Globals.h"
#include <SDL.h>

class Events
{
protected:
	SDL_Event Event;
	const Uint8 *state = nullptr;
	int mousex;
	int mousey;
public:
	Events()
	{
		state = SDL_GetKeyboardState(NULL);
	}
	int getMousex();
	int getMousey();
	void updateMouseCoordinates();
	bool handleInput();
	void update();
	~Events()
	{
	}
};