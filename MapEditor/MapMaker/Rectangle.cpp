#include "Globals.h"
#include "Rectangle.h"


Rectangle::Rectangle(SDL_Rect* size_and_position, SDL_Color* color)
{
	this->Texture = nullptr;
	this->Size_and_Position = size_and_position;
	this->Color = color;
	this->Texture = SDL_CreateTexture(Screen->getRenderer(), SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, SCREENX, SCREENY);
	if(this->Texture == nullptr)
	{
		logSDLError("Constructor Rectangle.cpp");
	}
	this->RenderingIndex = Screen->addRectangle(this);
}

void Rectangle::draw()
{
	SDL_SetRenderTarget(Screen->getRenderer(), this->Texture);
	SDL_SetRenderDrawColor(Screen->getRenderer(), 0x00, 0x00, 0x00, 0x00);
	SDL_RenderDrawRect(Screen->getRenderer(), this->Size_and_Position);
	SDL_SetRenderDrawColor(Screen->getRenderer(), this->Color->r, this->Color->g, this->Color->b, this->Color->a);
	SDL_RenderFillRect(Screen->getRenderer(), this->Size_and_Position);
	SDL_SetRenderTarget(Screen->getRenderer(), NULL);
	SDL_RenderCopy(Screen->getRenderer(), this->Texture, NULL, NULL);

	/*this->Size_and_Position->x = 0;
	this->Size_and_Position->y = 0;
	this->Size_and_Position->w = 10;
	this->Size_and_Position->h = 10;*/

	/*SDL_SetRenderTarget(Screen->getRenderer(), this->Texture);
	SDL_SetRenderDrawColor(Screen->getRenderer(), 0x00, 0x00, 0x00, 0x00);
	SDL_RenderClear(Screen->getRenderer());
	SDL_RenderDrawRect(Screen->getRenderer(), this->Size_and_Position);
	SDL_SetRenderDrawColor(Screen->getRenderer(), 0xFF, 0x00, 0x00, 0x00);
	SDL_RenderFillRect(Screen->getRenderer(), this->Size_and_Position);
	SDL_SetRenderTarget(Screen->getRenderer(), NULL);
	SDL_RenderCopy(Screen->getRenderer(), this->Texture, NULL, NULL);
	SDL_RenderPresent(Screen->getRenderer());*/

}

void Rectangle::destory()
{
	Screen->removeRectangle(this->RenderingIndex);
}

void Rectangle::editRectangle(SDL_Rect* size_and_position, SDL_Color* color)
{
	delete this->Size_and_Position;
	this->Size_and_Position = size_and_position;
	delete this->Color;
	this->Color = color;
	SDL_DestroyTexture(this->Texture);
	this->Texture = SDL_CreateTexture(Screen->getRenderer(), SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, this->Size_and_Position->w, this->Size_and_Position->h);
}

void Rectangle::editRectangle(SDL_Rect* size_and_position)
{
	delete this->Size_and_Position;
	this->Size_and_Position = size_and_position;
	SDL_DestroyTexture(this->Texture);
	this->Texture = SDL_CreateTexture(Screen->getRenderer(), SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, this->Size_and_Position->w, this->Size_and_Position->h);
}

void Rectangle::editRectangle(SDL_Color* color)
{
	delete this->Color;
	this->Color = color;
}

Rectangle::~Rectangle()
{
	SDL_DestroyTexture(this->Texture);
	delete this->Color;
	delete this->Size_and_Position;
}
