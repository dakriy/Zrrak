#pragma once

#include "Item.h"

class Entity : public Item
{
protected:
	int Speed;
	double Angle;
	int Health;
public:
	Entity(PictureFrame * info = nullptr, bool walkable = true, int speedmodifer = 0, int speed = 32);
	void setspeed(int speed);
	void setAngle(double angle);
	void setMovingAngle();
	void setHealth(int health);
	int getHealth();
	void move();
	~Entity();
};