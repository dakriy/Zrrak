#pragma once

#include <fstream>
#include <string>

using namespace std;
class ErrorHandler
{
public:
	ErrorHandler(string logFile);
	void LogError(string Type, string Error, string ErrorPlace);
protected:
	ofstream log;
	string file;
};