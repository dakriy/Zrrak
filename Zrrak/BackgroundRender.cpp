#include "BackgroundRender.h"
#include "Globals.h"
#include "Rectangle.h"

BackgroundRender::BackgroundRender()
{
	this->armorBar = nullptr;
	this->healthBar = nullptr;
	this->intilizeUI();
}

void BackgroundRender::intilizeUI()
{
	SDL_Rect * armor_rect = new SDL_Rect;
	armor_rect->x = 0;
	armor_rect->y = 10;
	armor_rect->h = 10;
	armor_rect->w = SCREENX;
	SDL_Color * armor_color = new SDL_Color;
	armor_color->a = 0;
	armor_color->r = 0;
	armor_color->g = 0;
	armor_color->b = 255;
	this->armorBar = new Rectangle(armor_rect, armor_color);

	SDL_Rect * health_rect = new SDL_Rect;
	health_rect->x = 0;
	health_rect->y = 0;
	health_rect->h = 10;
	health_rect->w = SCREENX;
	SDL_Color * health_color = new SDL_Color;
	health_color->a = 0;
	health_color->r = 0;
	health_color->g = 255;
	health_color->b = 0;
	this->healthBar = new Rectangle(health_rect, health_color);

	SDL_Rect * xp_rect = new SDL_Rect;
	xp_rect->x = 0;
	xp_rect->y = SCREENY - 10;
	xp_rect->h = 10;
	xp_rect->w = 0;
	SDL_Color * xp_color = new SDL_Color;
	xp_color->a = 0;
	xp_color->r = 255;
	xp_color->g = 255;
	xp_color->b = 0;
	this->xpBar = new Rectangle(xp_rect, xp_color);
}

void BackgroundRender::renderUI()
{
	this->xpBar->updateSizes(SCREENX * (double)player->getCurrentXP() / (double)player->getXPToLevel());
	this->healthBar->updateSizes((SCREENX * ((double)player->getHealth() / (double)player->getMaxHealth())));
	this->armorBar->updateSizes(player->getArmor());
}

BackgroundRender::~BackgroundRender()
{
	Screen->removeRectangle(this->armorBar);
	Screen->removeRectangle(this->healthBar);
	Screen->removeRectangle(this->xpBar);
}
