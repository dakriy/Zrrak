#include "Globals.h"
#include "Entity.h"

void Entity::setspeed(int speed)
{
	this->Speed = speed;
}

void Entity::setHealth(int health)
{
	this->Health = health;
}

int Entity::getHealth()
{
	return this->Health;
}

void Entity::move()
{
	double xchange = (SCREENX / 2) - this->Info->Position->x;
	double ychange = (SCREENY / 2) - this->Info->Position->y;
	int jumpsize = 3;
	if(xchange > 0)
	{
		this->Info->Position->x += jumpsize;
	}
	else
	{
		this->Info->Position->x -= jumpsize;
	}

	if(ychange > 0)
	{
		this->Info->Position->y += jumpsize;
	}
	else
	{
		this->Info->Position->y -= jumpsize;
	}
}

void Entity::setAngle(double angle)
{
	this->Angle = angle;
}

void Entity::setMovingAngle()
{
	// temporary variables to hold mouse coords
	int xCoord = this->Info->Position->x;
	int yCoord = this->Info->Position->y;

	// Set on cartesian plain so 0,0 is in the middle
	xCoord = (((float)xCoord) - SCREENX/2.0);
	yCoord = -(((float)yCoord) - SCREENY/2.0);

	// no division by 0
	if (xCoord > 0 && yCoord != 0)
	{
		this->Info->angle = 90 - (atan((float)yCoord / (float)xCoord) * (180 / PI));
	}
	if (xCoord == 0)
	{
		if (yCoord > 0)
		{
			this->Info->angle = 0;
		}
		else
		{
			this->Info->angle = 180;
		}
	}
	if (yCoord == 0)
	{
		if (xCoord > 0)
		{
			this->Info->angle = 90;
		}
		else
		{
			this->Info->angle = -90;
		}
	}
	if (xCoord < 0 && yCoord != 0)
	{
		this->Info->angle = 270 - (atan((float)yCoord / (float)xCoord) * (180 / PI));
	}
}

Entity::Entity(PictureFrame * info, bool walkable, int speedmodifer, int speed) : Item(info, walkable, speedmodifer)
{
	this->Speed = speed;
	this->setAngle(180);
	this->Health = 10 + (10 * Time_InMinutes);
}

Entity::~Entity()
{
	if (this->getRenderingIndex() != nullptr)
	{
		Screen->remove_without_delete(this->getInfo());
		delete this->Info;
		this->Info = nullptr;
	}
	else
	{
		if (this->Info != nullptr)
		{
			delete this->Info;
		}
	}
}
