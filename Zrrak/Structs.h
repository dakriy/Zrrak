#pragma once

#include "SDL.h"
#include <string>

class PictureFrame
{
public:
	PictureFrame(SDL_Texture * texture = nullptr);
	SDL_Texture * Texture;
	std::string type;
	SDL_Rect * Selection;
	SDL_Rect * Position;
	int * RenderingIndex;
	double angle;
	SDL_RendererFlip Flip;
	~PictureFrame();
};
