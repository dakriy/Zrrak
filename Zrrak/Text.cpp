#include "Text.h"
#include "Globals.h"

Text::Text()
{
	this->Info = new PictureFrame();
	this->fontSize = 28;
	this->Color = { 255, 255, 255 };
	this->fontType = "C:\\Windows\\Fonts\\arial.ttf";
	this->font = TTF_OpenFont(fontType.c_str(), fontSize);
	if (font == nullptr) {
		logTTFError("Text.cpp");
	}
}

void Text::setColor(SDL_Color color)
{
	this->Color = color;
}

void Text::setFontSize(int size)
{
	this->fontSize = size;
}

void Text::setMessage(string message)
{
	this->Message = message;
}

void Text::setFontType(string file)
{
	this->fontType = file;
	TTF_CloseFont(this->font);
	this->font = TTF_OpenFont(fontType.c_str(), fontSize);
	if (font == nullptr) 
	{
		logTTFError("main.cpp");
	}
}

void Text::Render(bool renderNoSize)
{
	SDL_Surface * text_surface = TTF_RenderText_Solid(this->font, this->Message.c_str(), this->Color);
	if (text_surface == nullptr) {
		logTTFError("main.cpp");
	}
	SDL_Texture *texture = SDL_CreateTextureFromSurface(Screen->getRenderer(), text_surface);
	if (texture == nullptr) {
		logSDLError("main.cpp");
	}
	SDL_FreeSurface(text_surface);

	this->Info->Texture = texture;

	if(this->Info->RenderingIndex == nullptr)
	{
		Screen->addTexture(this->Info);
	}
	else
	{
		Screen->remove_without_delete(this->Info);
		Screen->addTexture(this->Info);
	}
}

void Text::setSize(int x, int y, int w, int h)
{
	this->Info->Selection->x = x;
	this->Info->Selection->y = y;
	this->Info->Selection->w = w;
	this->Info->Selection->h = h;
}

void Text::setPlacement(int x, int y, int w, int h)
{
	this->Info->Position->x = x;
	this->Info->Position->y = y;
	this->Info->Position->w = w;
	this->Info->Position->h = h;
}

void Text::setPlacement(SDL_Rect* placement)
{
	this->Info->Position = placement;
}

PictureFrame* Text::getInfo()
{
	return this->Info;
}

Text::~Text()
{
	TTF_CloseFont(this->font);
	Screen->removeTexture(this->Info);
}

void Text::setSize(SDL_Rect* size)
{
	this->Info->Selection = size;
}