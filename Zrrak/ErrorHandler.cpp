#include "ErrorHandler.h"
#include "Map.h"

ErrorHandler::ErrorHandler(string logFile)
{
	this->file = logFile;
}

void ErrorHandler::LogError(string Type, string Error, string ErrorPlace)
{
	this->log.open(this->file);
	if (!this->log.is_open())
	{
		// Problems?
	}
	else
	{
		this->log << Type << " in " << ErrorPlace << ": " << endl << Error << endl;
	}
	this->log.close();
}
