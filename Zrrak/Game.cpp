#include "Game.h"
#include <SDL_image.h>
#include "Globals.h"
#include "Structs.h"

Game::Game()
{
	ZombieTime = 0;
}

// This updates the game state and spawns zombies and stuff
void Game::updateGame()
{
	Time_InMinutes = SDL_GetTicks() / 60000.0;
	this->spawn();
	for (int i = 0; i < Zombies.size(); i++)
	{
		if (secondsSinceLastWalk >= 10)
		{
			Zombies[i]->move();
			Zombies[i]->setMovingAngle();
			lastwalk = SDL_GetTicks();
		}
		secondsSinceLastWalk = SDL_GetTicks() - lastwalk;
	}
	int timeSinceLastHit = 100;
	int lastHit = 0;
	for (int i = 0; i < Zombies.size(); i++)
	{
		if (((Zombies[i]->getInfo()->Position->x) < (32 + SCREENX / 2)) && (Zombies[i]->getInfo()->Position->y) < (32 + SCREENY / 2) && (Zombies[i]->getInfo()->Position->y > SCREENY / 2 - 32) && ((Zombies[i]->getInfo()->Position->x) > SCREENX / 2 - 32))
		{
			if (timeSinceLastHit > 100)
			{
				player->setHealth(player->getHealth() - 10);
				lastHit = SDL_GetTicks();
			}
			timeSinceLastHit = SDL_GetTicks() - lastHit;
		}
	}
	if(walk)
	{
		/*int mousex;
		int mousey;
		SDL_GetMouseState(&mousex, &mousey);
		mousex -= SCREENX / 2;
		mousey -= SCREENY / 2;
		int jumpsize = 1;
		if (mousex > 0)
		{
			Design->updateCoords(jumpsize, 0);
		}
		else
		{
			Design->updateCoords(-jumpsize, 0);
		}

		if (mousey > 0)
		{
			Design->updateCoords(0, jumpsize);
		}
		else
		{
			Design->updateCoords(0, -jumpsize);
		}*/
	}
	if(clicked)
	{
		int range = player->getWeapon()->getRange() * 32;
		for (int i = 0; i < Zombies.size(); i++)
		{
			if (((Zombies[i]->getInfo()->Position->x) < (range + SCREENX / 2)) && (Zombies[i]->getInfo()->Position->y) < (range + SCREENY / 2) && (Zombies[i]->getInfo()->Position->y > SCREENY / 2 - range) && ((Zombies[i]->getInfo()->Position->x) > SCREENX / 2 - range))
			{
				Zombies[i]->setHealth(Zombies[i]->getHealth() - player->getDamage());
				break;
			}
		}
	}
	this->checkHealth();
}

void Game::spawn()
{
	// Randomly spawn zombies.
	// Chance is one in 10 to spawn a zombie every second
	if (Zombies.size() < 15 + (Time_InMinutes * 10))
	{
		if (((SDL_GetTicks() - ZombieTime) > 1000))
		{
			int chance = rand() % 10;
			if (chance == 0)
			{
				ZombieTime = SDL_GetTicks();
				PictureFrame * zombie = new PictureFrame;
				zombie->Flip = SDL_FLIP_NONE;
				zombie->Position = new SDL_Rect;
				zombie->Position->x = Design->getx_coords() + (rand() % SCREENX);
				zombie->Position->y = Design->gety_coords() + (rand() % SCREENY);
				zombie->Position->w = 32;
				zombie->Position->h = 32;
				zombie->type = "Monsters/Zombie.png";
				zombie->Texture = IMG_LoadTexture(Screen->getRenderer(), "d:\\Programming\\Git\ Projects\\Zrrak\\Zrrak\\Monsters\\Zombie.png");
				Zombies.push_back(new Entity(zombie));
				Zombies[Zombies.size() - 1]->setHealth(100 + (10 * Time_InMinutes));
				Screen->addTexture(Zombies[Zombies.size() - 1]->getInfo());
			}
		}
	}
}

void Game::checkHealth()
{
	if (player->getHealth() <= 0)
	{
		//Running = false;
	}
	for (int i = 0; i < Zombies.size(); i++)
	{
		if(Zombies[i]->getHealth() <= 0)
		{
			Zombies[i]->unRender();
			player->updateCurrentXP(50);
			Zombies.erase(Zombies.begin() + i);
		}
	}
}

Game::~Game()
{
	for (int i = 0; i < Zombies.size(); i++)
	{
		delete Zombies[i];
	}
	ZombiesToBeDeleted.clear();
	Zombies.clear();
}
