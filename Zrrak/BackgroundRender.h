#pragma once
#include "Rectangle.h"

class BackgroundRender
{
public:
	BackgroundRender();
	void intilizeUI();
	void renderUI();
	~BackgroundRender();
protected:
	Rectangle * healthBar;
	Rectangle * armorBar;
	Rectangle * xpBar;
};