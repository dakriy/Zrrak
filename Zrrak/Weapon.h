#pragma once

#include <string>
#include "SDL.h"
#include "Structs.h"

using namespace std;

class Weapon
{
protected:
	string Name;
	int Damage;
	int Range;
	int SpawnChance;
	PictureFrame * Info;
	long long int x_coords;
	long long int y_coords;
public:
	Weapon(string name = "", SDL_Texture* texture = nullptr, int range = NULL, int damage = NULL);
	void setDamage(int damage);
	void setRange(int range);
	void setTexutre(SDL_Texture * texture);
	void setName(string name);
	void Render();
	void destroy();
	int getDamage();
	int getRange();
	int getSpawnChance();
	void setPosition(long long int x, long long int y);
	string getName();
	void loadWeaponFromFile(char * fileName);
	~Weapon();
};