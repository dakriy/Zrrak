/*#include "Globals.h"
#include <iostream>
#include "Events.h"
#include "BackgroundRender.h"
#include "SDL_thread.h"
#include "SDL_ttf.h"
#include <time.h>
#include  "Game.h"

using namespace std;

static int Music(void *ptr)
{
}

void quit()
{
	TTF_Quit();
	SDL_Quit();
}

static int RenderGame(void *ptr)
{
	while (Running)
	{
		Screen->present();
	}
	return 0;
}

void initilization()
{

	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_EVENTS);
	TTF_Init();
	if (!TTF_WasInit())
	{
		logTTFError("main.cpp Initialization");
		quit();
		exit(EXIT_FAILURE);
	}
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_EVENTS) != 0)
	{
		logSDLError("main.cpp Initialization");
		quit();
		exit(EXIT_FAILURE);
	}
	srand(time(NULL));
}

int main(int argc, char* argv[])
{
	initilization();
	SDL_Window * window;

	window = SDL_CreateWindow(
		"Zrrak Map Maker",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		SCREENX,
		SCREENY,
		SDL_WINDOW_OPENGL
	);
	if (window == NULL || window == nullptr)
	{
		logSDLError("main.cpp");
		return 1;
	}
	
	Screen = new Render(window);
	Design = new Map();
	player = new Player();
	Game * game = new Game;
	bkg = new BackgroundRender;
	player->setPlayerTexutre(IMG_LoadTexture(Screen->getRenderer(), "d:\\Programming\\Git\ Projects\\Zrrak\\Zrrak\\Players\\Arlan_wood_sword.png"));
	player->setPlayerSection(0, 64, 32, 32);
	player->Render();
	Events * e = new Events;
	RenderThread = SDL_CreateThread(RenderGame, "RenderThread", nullptr);
	while (!e->handleInput())
	{
		player->updateAngle();
		bkg->renderUI();
		game->updateGame();
	}
	Running = false;
	int threadReturnValue;
	SDL_WaitThread(RenderThread, &threadReturnValue);
	//  created Pointers
	delete e;
	delete game;
	delete player;
	delete Screen;
	delete Design;
	SDL_DestroyWindow(window);
	quit();
	return 0;
}*/
#include <stdio.h>
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include <math.h>
#include <string>;
#include <string.h>
using namespace std;


#define PI 3.14159265
#define WIDTH 800
#define HEIGHT 600
#define IMG_PATH "d:/Programming/Git\ Projects/Zrrak/Zrrak/Players/players.png"
#define OTHER_PATH "d:/Programming/Git\ Projects/Zrrak/Zrrak/Players/Arlan_wood_sword.png"

int main(int argc, char *argv[]) {
	int player = 0;
	SDL_Window *win = NULL;
	SDL_Renderer *renderer = NULL;
	SDL_Texture *img = NULL;
	int w, h; // texture width & height

			  // Initialize SDL.
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		return 1;

	// create the window and renderer
	// note that the renderer is accelerated
	win = SDL_CreateWindow("Zrrak", 300, 300, WIDTH, HEIGHT, SDL_WINDOW_RESIZABLE);
	renderer = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);

	// load our image
	img = IMG_LoadTexture(renderer, IMG_PATH);
	SDL_QueryTexture(img, NULL, NULL, &w, &h); // get the width and height of the texture
											   // put the location where we want the texture to be drawn into a rectangle
											   // I'm also scaling the texture 2x simply by setting the width and height
											   // texture placement
	SDL_Rect texr;
	texr.w = 128;
	texr.h = 128;
	texr.x = (WIDTH / 2) - texr.w / 2;
	texr.y = (HEIGHT / 2) - texr.h / 2;

	// texture piece
	SDL_Rect SrcR;
	SrcR.x = 0;
	SrcR.y = 0 + 32 * player;
	SrcR.w = 32;
	SrcR.h = 32;

	int wit;
	int hei;
	SDL_Texture * otherimg = IMG_LoadTexture(renderer, OTHER_PATH);
	SDL_QueryTexture(img, NULL, NULL, &wit, &hei);
	SDL_Rect texra;
	texra.w = 128;
	texra.h = 128;
	texra.x = (WIDTH / 2) - texr.w / 2;
	texra.y = (HEIGHT / 2) - texr.h / 2;

	// texture piece
	SDL_Rect SrcRa;
	SrcRa.x = 0;
	SrcRa.y = 32;
	SrcRa.w = 32;
	SrcRa.h = 32;

	// event handling
	SDL_Event e;

	int mousex;
	int mousey;

	unsigned int time_since_last_frame = 0;
	unsigned int frame = 0;
	float frames_passed = 0;
	//float frames_per_second = 0;
	float angle = 0;
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	bool walk = false;
	int section = 0;
	int time_since_last_player_walk_section = 0;
	int time_since_last_player_swing_section = 0;
	bool mouse = false;
	int last_walk_section = 0;
	int last_swing_section = 0;
	int sectiona = 0;
	// main loop
	while (1) {
		if (SDL_PollEvent(&e)) {
			if (e.type == SDL_QUIT)
				break;
			else if (e.type == SDL_KEYUP && e.key.keysym.sym == SDLK_ESCAPE)
				break;
			if (e.type == SDL_MOUSEBUTTONDOWN)
				mouse = true;
			else
				mouse = false;
		}
		if (state[SDL_SCANCODE_W])
		{
			walk = true;
		}
		else
		{
			walk = false;
			section = 0;
		}
		if (walk)
		{
			time_since_last_player_walk_section = SDL_GetTicks() - last_walk_section;
			if (time_since_last_player_walk_section > 100)
			{
				if (section == 3)
				{
					section = 0;
				}
				else
				{
					section++;
				}
				last_walk_section = SDL_GetTicks();
				time_since_last_player_walk_section = 0;
			}
		}

		if(mouse)
		{
			time_since_last_player_swing_section = SDL_GetTicks() - last_swing_section;
			if (time_since_last_player_swing_section > 100)
			{
				if (sectiona == 3)
				{
					sectiona = 0;
				}
				else
				{
					sectiona++;
				}
				last_swing_section = SDL_GetTicks();
				time_since_last_player_swing_section = 0;
			}
		}

		if (time_since_last_frame > 16.5)
		{
			SDL_GetMouseState(&mousex, &mousey);
			mousex = (((float)mousex) - 400);
			mousey = -(((float)mousey) - 300);
			if (mousex > 0 && mousey != 0)
			{
				angle = 90 - (atan((float)mousey / (float)mousex) * (180 / PI));
			}
			if (mousex == 0)
			{
				if (mousey > 0)
				{
					angle = 0;
				}
				else
				{
					angle = 180;
				}
			}
			if (mousey == 0)
			{
				if (mousex > 0)
				{
					angle = 90;
				}
				else
				{
					angle = -90;
				}
			}
			if (mousex < 0 && mousey != 0)
			{
				angle = 270 - (atan((float)mousey / (float)mousex) * (180 / PI));
			}
			if (mouse)
			{
				SrcRa.x = 0 + 32 * sectiona;
			} else
			{
				SrcR.x = 0 + 32 * section;
			}
			frame = SDL_GetTicks();
			// clear the screen
			SDL_RenderClear(renderer);
			// copy the texture to the rendering context
			if (mouse)
			{
				SDL_RenderCopyEx(renderer, otherimg, &SrcRa, &texra, angle, NULL, SDL_FLIP_VERTICAL);
			} else
			{
				SDL_RenderCopyEx(renderer, img, &SrcR, &texr, angle, NULL, SDL_FLIP_VERTICAL);
			}
			// flip the backbuffer
			// this means that everything that we prepared behind the screens is actually shown
			SDL_RenderPresent(renderer);
			frames_passed++;
			double frames_per_second = frames_passed / (SDL_GetTicks() / 1000);
			system("cls");
			cout << "x,y mouse coordinates: " << mousex << "," << mousey << endl;
			cout << "player turning angle: " << angle << endl;
			cout << "Frames per second: " << frames_per_second << endl;
		}
		time_since_last_frame = SDL_GetTicks() - frame;
	}

	SDL_DestroyTexture(img);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(win);
	SDL_DestroyTexture(otherimg);
	SDL_Quit();

	return 0;
}