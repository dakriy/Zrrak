﻿#pragma once

#include "SDL.h"
#include "Structs.h"

class Item
{
public:
	// Do not bother deleting anything that gets passed to the constructor as it will be autmoatically deleted later
	Item(PictureFrame * info = nullptr, bool walkable = true, int speedmodifer = 0);
	SDL_Rect * getPlacement();
	void setInfo(PictureFrame * info);
	void setPlacement(SDL_Rect * position);
	void setRenderingIndex(int * index);
	int * getRenderingIndex();
	void setTexture(SDL_Texture * texture);
	SDL_Texture * getTexture();
	SDL_Rect * getSelection();
	bool getWalkable();
	int getSpeedModifier();
	PictureFrame * getInfo();
	void unRender();
	~Item();
protected:
	PictureFrame * Info;
	bool Traversable;
	int SpeedModifier;
};