#include "Globals.h"
#include "ErrorHandler.h"
#include "SDL_ttf.h"
#include "dirent.h"

#ifndef SCREEN_RENDERER
#define SCREEN_RENDERER
	bool Running = true;
	SDL_Thread * RenderThread = nullptr;
	Render * Screen = nullptr;
	Map * Design = nullptr;
	Player * player = nullptr;
	BackgroundRender * bkg = nullptr;
	bool walk = false;
	bool clicked = false;
	int Time_InMinutes = 0;
	std::vector<string> TileNames;
	std::vector<string> EntityNames;
#endif
void logTTFError(string currentFile)
{
	ErrorHandler * Error = new ErrorHandler(LOGFILE);
	Error->LogError("TTF ERROR", TTF_GetError(), currentFile);
	delete Error;
}

void logSDLError(string currentFile)
{
	ErrorHandler * Error = new ErrorHandler(LOGFILE);
	Error->LogError("SDL ERROR", SDL_GetError(), currentFile);
	delete Error;
}

void logError(string type, string currentFile, string error)
{
	ErrorHandler * Error = new ErrorHandler(LOGFILE);
	Error->LogError(type, error, currentFile);
	delete Error;
}
