﻿#include "Globals.h"
#include "Map.h"
#include <time.h>

Map::Map()
{
	this->x_coords = 0;
	this->y_coords = 0;
}

int Map::getx_coords()
{
	return this->x_coords;
}

int Map::gety_coords()
{
	return this->y_coords;
}

void Map::addItem(Item* item_to_add)
{
	Tiles.push_back(item_to_add);
}

void Map::removeItem(Item* item)
{
	// Go through to find the one to be removed
	for (int i = 0; i < Tiles.size(); i++)
	{
		if (item == Tiles[i])
		{
			delete Tiles[i];
			Tiles[i] = nullptr;
			Tiles.erase(Tiles.begin() + i);
			break;
		}
	}
}

// method renders the tiles and entities vectors
void Map::renderMap()
{
	// This function is going to suck.
	// I MUST COMMENT THIS OTHERWISE I WILL SPED AN HOUR MORE THAN I NEEDED TO
	// OK... FOCUS >.>
	//
	//
	// So how this works. (I need to write this down so I don't forget)
	// first it finds all the textures that are loaded by looking at the placment
	// variable in all the items and unloading them if it exists.
	// then it determines which ones to load based on coordinates in the item
	// and loads those into the renderer to be rendered


	this->generate();
	int allowed_x_u = this->x_coords + (SCREENX + 1);
	int allowed_x_l = this->x_coords - 1;
	int allowed_y_u = this->y_coords + (SCREENY + 1);
	int allowed_y_l = this->y_coords - 1;

	for (int i = 0; i < Tiles.size(); i++)
	{
		SDL_Rect TilePlacement = *Tiles[i]->getPlacement();
		if (((TilePlacement.x > allowed_x_l) && (TilePlacement.x < allowed_x_u)) && ((TilePlacement.y > allowed_y_l) && (TilePlacement.y < allowed_y_u)))
		{
			if(this->Tiles[i]->getInfo()->RenderingIndex == nullptr)
			{
				Screen->addTile(Tiles[i]->getInfo());
			}
		} else
		{
			if(Tiles[i]->getRenderingIndex() != nullptr)
			{
				Screen->removeTileWithoutDelete(Tiles[i]->getInfo());
				delete this->Tiles[i];
			}
		}
	}
	for (int i = 0; i < Entities.size(); i++)
	{
		SDL_Rect TilePlacement = *Entities[i]->getPlacement();
		if (((TilePlacement.x > allowed_x_l) && (TilePlacement.x < allowed_x_u)) && ((TilePlacement.y > allowed_y_l) && (TilePlacement.y < allowed_y_u)))
		{
			if(this->Entities[i]->getInfo()->RenderingIndex == nullptr)
			{
				Screen->addTexture(Entities[i]->getInfo());
			}
		}
		else
		{
			if(Entities[i]->getRenderingIndex() != nullptr)
			{
				Screen->remove_without_delete(Entities[i]->getInfo());
				delete this->Entities[i];
			}
		}
	}
}

// Function populates tiles vector
void Map::generate()
{
	// This method is going to generate new tiles for the map.
	// :(
	if(!Running)
	{
		return;
	}
	int ChunkSizex = SCREENX / 2.0;
	int ChunkSizey = SCREENY / 2.0;
	// Clean out not needed tiles
	if(Tiles.size() == 0)
	{
		// generate in rows
		for (int z = 0; z < SCREENY; z += ChunkSizey)
		{
			// for each chunk
			// SCREENX is rendering size
			// this is not going to work currently
			for (int i = 0; i < SCREENX; i += ChunkSizex)
			{
				for (int theta /*Yay for greek letters!*/ = 0; theta <= ChunkSizey; theta += 32)
				{
					for (int sigma = 0; sigma <= ChunkSizex; sigma += 32)
					{
						// generate chunk
						// random numbers from 0 to 9;
						int RandomNumber = rand() % 11;
						SDL_Texture * Biome = nullptr;
						SDL_Rect * Position = new SDL_Rect;
						bool walkable = true;
						int speedModifier = 0;
						string type;
						if(RandomNumber >= 6)
						{
							type = "tiles/grass.png";
						}
						if(RandomNumber == 4 || RandomNumber == 0)
						{
							type = "tiles/grass.png";
							walkable = false;
						}
						if (RandomNumber == 3)
						{
							type = "tiles/grass.png";
							speedModifier = -5;
						}
						if (RandomNumber == 2)
						{
							type = "tiles/grass.png";
							speedModifier = -20;
						}
						if (RandomNumber == 1 || RandomNumber == 5)
						{
							type = "tiles/tree.png";
							speedModifier = 10;
						}
						Biome = IMG_LoadTexture(Screen->getRenderer(), type.c_str());
						Position->x = (i + sigma);
						Position->y = (z + theta);
						Position->h = 32;
						Position->w = 32;
						PictureFrame * info = new PictureFrame(Biome);
						info->Position = Position;
						info->type = type;
						this->Tiles.push_back(new Item(info, walkable, speedModifier));
						Biome = nullptr;
					}
				}
			}
		}
	} else
	{
		for (int z = this->y_coords; z < (this->y_coords + SCREENY); z += ChunkSizey)
		{
			for (int i = this->x_coords; i < (this->x_coords + SCREENX); i += ChunkSizex)
			{
				for (int theta = 0; theta <= ChunkSizey; theta += 32)
				{
					for (int sigma = 0; sigma <= ChunkSizex; sigma += 32)
					{
						bool found = false;
						for (int n = 0; n < Tiles.size(); n++)
						{
							SDL_Rect placement;
							if(Tiles[n] != nullptr)
							{
								 placement = *Tiles[n]->getPlacement();
								 if (placement.x == sigma && placement.y == theta)
								 {
									 found = true;
									 break;
								 }
							}
						}
						if (!found)
						{
							// random numbers from 0 to 9;
							int RandomNumber = rand() % 10;
							SDL_Texture * Biome = nullptr;
							SDL_Rect * Position = new SDL_Rect;
							bool walkable = true;
							int speedModifier = 0;
							string type;
							if (RandomNumber >= 6)
							{
								type = "tiles/grass.png";
							}
							if (RandomNumber == 4 || RandomNumber == 0)
							{
								type = "tiles/grass.png";
								walkable = false;
							}
							if (RandomNumber == 3)
							{
								type = "tiles/grass.png";
								speedModifier = -5;
							}
							if (RandomNumber == 2)
							{
								type = "tiles/grass.png";
								speedModifier = -20;
							}
							if (RandomNumber == 1 || RandomNumber == 5)
							{
								type = "tiles/tree.png";
								speedModifier = 10;
							}
							Biome = IMG_LoadTexture(Screen->getRenderer(), type.c_str());
							Position->x = (i + sigma);
							Position->y = (z + theta);
							Position->h = 32;
							Position->w = 32;
							PictureFrame * info = new PictureFrame(Biome);
							info->Position = Position;
							info->type = type;
							this->Tiles.push_back(new Item(info, walkable, speedModifier));
							found = false;
							Biome = nullptr;
						}
					}
				}
			}
		}
	}
}

void Map::updateCoords(int deltax, int deltay)
{
	this->x_coords += deltax;
	this->y_coords += deltay;
}

void Map::replaceItem(Item* item, Item* new_item)
{
	this->removeItem(item);
	this->addItem(new_item);
}

void Map::addEntity(Entity* entity_to_add)
{
	Entities.push_back(entity_to_add);
}

void Map::removeEntity(Entity* entity)
{
	// Go through to find the one to be removed
	for (int i = 0; i < Entities.size(); i++)
	{
		if (entity == Entities[i])
		{
			delete Entities[i];
			Entities[i] = nullptr;
			Entities.erase(Entities.begin() + i);
			break;
		}
	}
}

Map::~Map()
{
	for (int i = 0; i < Tiles.size(); i++)
	{
		if (Tiles[i] != nullptr)
		{
			delete Tiles[i];
		}
	}

	for (int i = 0; i < Entities.size(); i++)
	{
		if(Entities[i] != nullptr)
		{
			delete Entities[i];
		}
	}
}
