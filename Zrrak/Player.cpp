#include "Player.h"
#include "Globals.h"
#include "Weapon.h"

void Player::levelUp()
{
	this->Level++;
	this->CurrentXP -= this->XPToLevel;
	// Magic number. If game is too hard reduce the magic number
	this->XPToLevel = this->XPToLevel + getLevel() * 30;
	this->MaxHealth += (100 + getLevel() * 10);
	this->MaxManna += (50 + getLevel() * 5);
	this->BaseDamage += (10 * getLevel());
	this->resetHeath();
	this->resetManna();
}

Player::Player()
{
	// Starting values. Too lazy to base difficulty off of this so.... Meh.
	this->Info = new PictureFrame;
	this->BaseDamage = 10;
	this->Info->Flip = SDL_FLIP_VERTICAL;
	this->CurrentXP = 0;
	this->Level = 1;
	this->MaxHealth = 100;
	this->MaxManna = 50;
	this->resetHeath();
	this->resetHeath();
	this->Alive = true;
	this->Armor = 100;
	this->Score = 0;
	this->setSpeed(32);
	this->Info->angle = 0;
	this->XPToLevel = 100;
	this->weapon = new Weapon;
	this->weapon->loadWeaponFromFile("d:\\Programming\\Git\ Projects\\Zrrak\\Zrrak\\Weapons\\Wooden\ Sword.weapon");
}

void Player::updateAngle()
{
	// temporary variables to hold mouse coords
	int mousex;
	int mousey;

	// get mouse coordinates
	SDL_GetMouseState(&mousex, &mousey);
	// Set on cartesian plain so 0,0 is in the middle
	mousex = (((float)mousex) - SCREENX/2.0);
	mousey = -(((float)mousey) - SCREENY/2.0);

	// no division by 0
	if (mousex > 0 && mousey != 0)
	{
		this->Info->angle = 90 - (atan((float)mousey / (float)mousex) * (180 / PI));
	}
	if (mousex == 0)
	{
		if (mousey > 0)
		{
			this->Info->angle = 0;
		}
		else
		{
			this->Info->angle = 180;
		}
	}
	if (mousey == 0)
	{
		if (mousex > 0)
		{
			this->Info->angle = 90;
		}
		else
		{
			this->Info->angle = -90;
		}
	}
	if (mousex < 0 && mousey != 0)
	{
		this->Info->angle = 270 - (atan((float)mousey / (float)mousex) * (180 / PI));
	}
}

int Player::getDamage()
{
	if(weapon == nullptr)
	{
		return this->BaseDamage;
	}
	return this->BaseDamage + this->weapon->getDamage();
}

void Player::setPlayerSection(int x, int y, int h, int w)
{
	this->Info->Selection = new SDL_Rect;
	this->Info->Selection->x = x;
	this->Info->Selection->y = y;
	this->Info->Selection->w = w;
	this->Info->Selection->h = h;
}

void Player::setHealth(int amountToChangeHealthBy)
{
	this->Health = amountToChangeHealthBy;
}

void Player::resetHeath()
{
	this->Health = this->MaxHealth;
}

int Player::getHealth()
{
	return this->Health;
}

void Player::setSpeed(int speed)
{
	this->Speed = speed;
}

int Player::getSpeed()
{
	return this->Speed;
}

int Player::getMaxHealth()
{
	return this->MaxHealth;
}

int Player::getArmor()
{
	return this->Armor;
}

Weapon* Player::getWeapon()
{
	return this->weapon;
}

void Player::updateManna(int AmountToChangeMannaBy)
{
	this->Mana += AmountToChangeMannaBy;
}

void Player::resetManna()
{
	this->Mana = this->MaxManna;
}

void Player::updateCurrentXP(int AmountToChangeXPBy)
{
	if (AmountToChangeXPBy > 0)
	{
		this->CurrentXP += AmountToChangeXPBy;
	}
	if (this->CurrentXP >= this->XPToLevel)
	{
		levelUp();
	}
}

int Player::getXPToLevel()
{
	return this->XPToLevel;
}

int Player::getCurrentXP()
{
	return this->CurrentXP;
}

int Player::getLevel()
{
	return this->Level;
}

void Player::updateScore(int AmountToChangeScoreBy)
{
	if (AmountToChangeScoreBy > 0)
	{
		this->Score += AmountToChangeScoreBy;
	}
}

void Player::Render()
{
	if (this->Info->RenderingIndex == nullptr)
	{
		Screen->addTexture(this->Info);
	}
}

void Player::Destroy()
{
	if (this->Info != nullptr)
	{
		Screen->removeTexture(this->Info);
	}
	delete weapon;
}

void Player::setPlayerTexutre(SDL_Texture* texture)
{
	this->Info->Texture = texture;
	this->Info->Position = new SDL_Rect;
	this->Info->Position->x = SCREENX / 2 - 16;
	this->Info->Position->y = SCREENY / 2 - 16;
	this->Info->Position->h = 32;
	this->Info->Position->w = 32;
}

bool Player::isAlive()
{
	return this->Alive;
}

double Player::getAngle()
{
	return this->Info->angle;
}

Player::~Player()
{
	if(this->Info->RenderingIndex != nullptr)
	{
		this->Destroy();
	}
	else
	{
		if (this->Info != nullptr)
		{
			delete Info;
		}
	}
}
