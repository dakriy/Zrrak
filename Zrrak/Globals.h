#pragma once


#include <string>
#include <SDL.h>
#include "Render.h"
#include "Map.h"
#include "Player.h"
#include "BackgroundRender.h"

#ifndef INCLUDES_HEADERS
#define INCLUDES_HEADERS
using namespace std;
	#define SCREENX 1366
	#define SCREENY 768
	#define LOGFILE "log.txt"
	#define PI 3.14159265
	extern bool Running;
	extern int Time_InMinutes;
	extern SDL_Thread * RenderThread;
	extern BackgroundRender * bkg;
	extern Render * Screen;
	extern Map * Design;
	extern Player * player;
	extern bool clicked;
	extern bool walk;
	extern void logTTFError(string currentFile);
	extern void logSDLError(string currentFile);
	extern void logError(string type, string currentFile, string error);
#endif