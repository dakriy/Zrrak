#pragma once

#include "SDL.h"
#include "Structs.h"

class Rectangle
{
protected:
	SDL_Texture * Texture;
	SDL_Rect * Size_and_Position;
	SDL_Color * Color;
	int * RenderingIndex;
public:
	Rectangle(SDL_Rect * size_and_position, SDL_Color * color);
	void draw();
	void destory();
	void setRenderingIndex(int * index);
	void editRectangle(SDL_Rect * size_and_position, SDL_Color * color);
	void editRectangle(SDL_Rect * size_and_position);
	void editRectangle(SDL_Color * color);
	void updateSizes(int NewSize);
	int * getRenderingIndex();
	~Rectangle();
};