#include "Globals.h"
#include "Rectangle.h"


Rectangle::Rectangle(SDL_Rect* size_and_position, SDL_Color* color)
{
	this->Texture = nullptr;
	this->Size_and_Position = size_and_position;
	this->Color = color;
	this->Texture = SDL_CreateTexture(Screen->getRenderer(), SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, SCREENX, SCREENY);
	if(this->Texture == nullptr)
	{
		logSDLError("Constructor Rectangle.cpp");
	}
	Screen->addRectangle(this);
}

void Rectangle::draw()
{
	SDL_SetRenderDrawColor(Screen->getRenderer(), this->Color->r, this->Color->g, this->Color->b, this->Color->a);
	SDL_RenderFillRect(Screen->getRenderer(), this->Size_and_Position);
}

void Rectangle::destory()
{
	Screen->removeRectangle(this);
}

void Rectangle::setRenderingIndex(int* index)
{
	this->RenderingIndex = index;
}

void Rectangle::editRectangle(SDL_Rect* size_and_position, SDL_Color* color)
{
	delete this->Size_and_Position;
	this->Size_and_Position = size_and_position;
	delete this->Color;
	this->Color = color;
	SDL_DestroyTexture(this->Texture);
	this->Texture = SDL_CreateTexture(Screen->getRenderer(), SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, this->Size_and_Position->w, this->Size_and_Position->h);
}

void Rectangle::editRectangle(SDL_Rect* size_and_position)
{
	delete this->Size_and_Position;
	this->Size_and_Position = size_and_position;
	SDL_DestroyTexture(this->Texture);
	this->Texture = SDL_CreateTexture(Screen->getRenderer(), SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, this->Size_and_Position->w, this->Size_and_Position->h);
}

void Rectangle::editRectangle(SDL_Color* color)
{
	delete this->Color;
	this->Color = color;
}

void Rectangle::updateSizes(int NewSize)
{
	this->Size_and_Position->w = NewSize;
}

int* Rectangle::getRenderingIndex()
{
	return this->RenderingIndex;
}

Rectangle::~Rectangle()
{
	SDL_DestroyTexture(this->Texture);
	delete this->Color;
	delete this->Size_and_Position;
	delete RenderingIndex;
}
