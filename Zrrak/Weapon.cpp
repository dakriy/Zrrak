#include "Weapon.h"
#include "Globals.h"
#include <fstream>
#include <string>

Weapon::Weapon(string name, SDL_Texture* texture, int range, int damage)
{
	this->setName(name);
	this->Info = new PictureFrame;
	this->setTexutre(texture);
	this->setRange(range);
	this->setDamage(damage);
}

void Weapon::setDamage(int damage)
{
	this->Damage = damage;
}

void Weapon::setRange(int range)
{
	this->Range = range;
}

void Weapon::setTexutre(SDL_Texture* texture)
{
	this->Info->Texture = texture;
}

void Weapon::Render()
{
	Screen->addTexture(this->Info);
}

void Weapon::destroy()
{
	Screen->removeTexture(this->Info);
}

void Weapon::setName(string name)
{
	this->Name = name;
}

int Weapon::getDamage()
{
	return this->Damage;
}

int Weapon::getRange()
{
	return this->Range;
}

int Weapon::getSpawnChance()
{
	return this->SpawnChance;
}

void Weapon::setPosition(long long int x, long long int y)
{
	this->x_coords = x;
	this->y_coords = y;
}

string Weapon::getName()
{
	return this->Name;
}

void Weapon::loadWeaponFromFile(char* fileName)
{
	fstream file(fileName, ios::in | ios::binary | ios::ate);
	if (!file.is_open())
	{
		logError("File I/O", "Weapon.cpp", "Failed to open Weapon File");
		return;
	}
	streampos size;
	size = file.tellg();
	char * length = new char[size];
	file.seekg(0, ios::beg);
	file.read(length, size);
	int TitleLength;
	memcpy(&TitleLength, length, sizeof(int));
	for (int i = sizeof(int); i < TitleLength + sizeof(int); i++)
	{
		this->Name += length[i];
	}
	memcpy(&this->Damage, (length + TitleLength + sizeof(int)), sizeof(int));
	memcpy(&this->Range, (length + TitleLength + sizeof(int) + sizeof(int)), sizeof(int));
	memcpy(&this->SpawnChance, (length + sizeof(int) + TitleLength + sizeof(int) + sizeof(int)), sizeof(int));
	delete length;
	file.close();
}

Weapon::~Weapon()
{
	this->destroy();
}
