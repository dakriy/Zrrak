#pragma once

#include "Item.h";
#include "Entity.h"
#include <vector>

using namespace std;

class Map
{
protected:
	string name;
	vector<Item *> Tiles;
	vector<Entity *> Entities;
	int x_coords;
	int y_coords;
public:
	Map();
	void addItem(Item * item_to_add);
	int getx_coords();
	int gety_coords();
	void removeItem(Item* item);
	void renderMap();
	void generate();
	void updateCoords(int deltax, int deltay);
	void replaceItem(Item* item, Item* new_item);
	void addEntity(Entity * entity_to_add);
	void removeEntity(Entity * entity);
	~Map();
};
