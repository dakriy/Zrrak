#pragma once

#include "SDL.h"
#include <SDL_image.h>
#include <vector>
#include "Rectangle.h"
#include "BackgroundRender.h"
#include "Structs.h"

using namespace std;

// Any pointers passed will be destroyed
class Render
{
protected:
	SDL_Renderer * Renderer;
	vector<PictureFrame *> Textures;
	vector<Rectangle *> Rectangles;
	//BackgroundRender * Background;
	vector<PictureFrame *> ToRemove;
	vector<Rectangle *> RectToRemove;
	vector<PictureFrame *> ToAdd;
	vector<Rectangle *> RectToAdd;
	vector<PictureFrame *> ToRemoveWODel;
	vector<PictureFrame *> Tiles;
	vector<PictureFrame *> TilesToRemove;
	vector<PictureFrame *> TilesToAdd;
	vector<PictureFrame *> TilesToRemoveWODel;
public:
	Render(SDL_Window * window);
	void display();
	void draw();
	void clearScreen();
	void present();
	// returns locaton in the vector
	void addTexture(PictureFrame * Info);
	void removeTexture(PictureFrame * Info);
	void replaceTexture(PictureFrame * OldInfo, SDL_Texture * NewTexture);
	SDL_Renderer * getRenderer();
	void addRectangle(Rectangle * rect);
	void removeRectangle(Rectangle * Info);
	void remove_without_delete(PictureFrame * Info);
	void addTile(PictureFrame * Info);
	void removeTile(PictureFrame * Info);
	void removeTileWithoutDelete(PictureFrame * Info);
	//void setBackground(BackgroundRender * bkg);
	void update();
	~Render();
};