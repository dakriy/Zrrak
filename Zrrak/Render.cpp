#include "Globals.h"
#include "SDL.h"
#include <SDL_image.h>
#include "Render.h"
#include "BackgroundRender.h"

using namespace std;

Render::Render(SDL_Window* window)
{
	this->Renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
}

void Render::clearScreen()
{
	SDL_RenderClear(this->Renderer);
}

void Render::present()
{
	if (Design != nullptr)
	{
		// puts current map tiles on the corect position on the screen.
		Design->renderMap();
	}
	this->update();
	this->clearScreen();
	this->draw();
	this->display();
}

void Render::draw()
{
	for (int i = 0; i < this->Tiles.size(); i++)
	{
		SDL_SetRenderTarget(this->Renderer, this->Tiles[i]->Texture);
		SDL_RenderCopyEx(this->Renderer, this->Tiles[i]->Texture, this->Tiles[i]->Selection, this->Tiles[i]->Position, this->Tiles[i]->angle, NULL, this->Tiles[i]->Flip);
	}
	for (int i = 0; i < Rectangles.size(); i++)
	{
		bkg->renderUI();
		Rectangles[i]->draw();
	}
	for (int i = 0; i < this->Textures.size(); i++)
	{
		SDL_SetRenderTarget(this->Renderer, this->Textures[i]->Texture);
		SDL_RenderCopyEx(this->Renderer, this->Textures[i]->Texture, this->Textures[i]->Selection, this->Textures[i]->Position, this->Textures[i]->angle, NULL, this->Textures[i]->Flip);
	}
}

void Render::display()
{
	SDL_RenderPresent(this->Renderer);
}

void Render::addTexture(PictureFrame * Info)
{
	this->ToAdd.push_back(Info);
}

// Deletes the index int pointer as well as the texture
void Render::removeTexture(PictureFrame * Info)
{
	ToRemove.push_back(Info);
}

void Render::replaceTexture(PictureFrame * OldInfo, SDL_Texture * NewTexture)
{
	if (*OldInfo->RenderingIndex >= 0 && *OldInfo->RenderingIndex < Textures.size())
	{
		OldInfo->Texture = NewTexture;
	}
}

SDL_Renderer* Render::getRenderer()
{
	return this->Renderer;
}

// User has to delete index pointer if this is manually used.
void Render::addRectangle(Rectangle* rect)
{
	this->Rectangles.push_back(rect);
}

void Render::removeRectangle(Rectangle * rect)
{
	RectToRemove.push_back(rect);
}

void Render::remove_without_delete(PictureFrame * Info)
{
	ToRemoveWODel.push_back(Info);
}

void Render::removeTile(PictureFrame* Info)
{
	this->TilesToRemove.push_back(Info);
}

void Render::removeTileWithoutDelete(PictureFrame* Info)
{
	this->TilesToRemoveWODel.push_back(Info);
}

void Render::addTile(PictureFrame* Info)
{
	this->TilesToAdd.push_back(Info);
}

/*void Render::setBackground(BackgroundRender* bkg)
{
	this->Background = bkg;
}*/

void Render::update()
{

	for (int i = 0; i < TilesToAdd.size(); i++)
	{
		Tiles.push_back(TilesToAdd[i]);
		int * index = new int;
		*index = Tiles.size() - 1;
		TilesToAdd[i]->RenderingIndex = index;
	}
	TilesToAdd.clear();

	// Handle everything to be removed
	for (int n = 0; n < TilesToRemove.size(); n++)
	{
		// If it's in the vector
		if (*TilesToRemove[n]->RenderingIndex < Tiles.size())
		{
			// loop through and change all other rendering indexes
			for (int z = *TilesToRemove[n]->RenderingIndex; z < Tiles.size(); z++)
			{
				*Tiles[z]->RenderingIndex -= 1;
			}
			// Erase it from the rendering vector
			this->Tiles.erase(Tiles.begin() + *TilesToRemove[n]->RenderingIndex);
			// delete it
			delete this->Tiles[*TilesToRemove[n]->RenderingIndex];
		}
	}
	TilesToRemove.clear();

	for (int i = 0; i < TilesToRemoveWODel.size(); i++)
	{
		if (*TilesToRemoveWODel[i]->RenderingIndex < Tiles.size())
		{
			// loop through and change all other rendering indexes
			for (int z = *TilesToRemoveWODel[i]->RenderingIndex; z < Tiles.size(); z++)
			{
				*Tiles[z]->RenderingIndex -= 1;
			}
			// Erase texture reference from rendering vector
			this->Tiles.erase(Tiles.begin() + *TilesToRemoveWODel[i]->RenderingIndex);
		}
	}
	TilesToRemoveWODel.clear();


	// Handle everything to be removed
	for (int n = 0; n < ToRemove.size(); n++)
	{
		// If it's in the vector
		if (*ToRemove[n]->RenderingIndex < Textures.size())
		{
			// loop through and change all other rendering indexes
			for (int z = *ToRemove[n]->RenderingIndex; z < Textures.size(); z++)
			{
				*Textures[z]->RenderingIndex -= 1;
			}
			// Erase it from the rendering vector
			this->Textures.erase(Textures.begin() + *ToRemove[n]->RenderingIndex);
			// delete it
			delete this->Textures[*ToRemove[n]->RenderingIndex];
		}
	}
	ToRemove.clear();
	for (int n = 0; n < RectToRemove.size(); n++)
	{
		delete this->Rectangles[*RectToRemove[n]->getRenderingIndex()];
		RectToRemove[n]->setRenderingIndex(nullptr);
	}
	RectToRemove.clear();

	for (int i = 0; i < ToAdd.size(); i++)
	{
		Textures.push_back(ToAdd[i]);
		int * index = new int;
		*index = Textures.size() - 1;
		ToAdd[i]->RenderingIndex = index;
	}
	ToAdd.clear();

	for (int i = 0; i < RectToAdd.size(); i++)
	{
		Rectangles.push_back(RectToAdd[i]);
		int * index = new int;
		*index = Rectangles.size() - 1;
		RectToAdd[i]->setRenderingIndex(index);
	}
	RectToAdd.clear();

	for (int i = 0; i < ToRemoveWODel.size(); i++)
	{
		if (*ToRemoveWODel[i]->RenderingIndex < Textures.size())
		{
			// loop through and change all other rendering indexes
			for (int z = *ToRemoveWODel[i]->RenderingIndex; z < Textures.size(); z++)
			{
				*Textures[z]->RenderingIndex -= 1;
			}
			// Erase texture reference from rendering vector
			this->Textures.erase(Textures.begin() + *ToRemoveWODel[i]->RenderingIndex);
		}
	}
	ToRemoveWODel.clear();
}

Render::~Render()
{
	/*for (auto i = 0; i < Textures.size(); i++)
	{
		delete Textures[i];
		Textures[i] = nullptr;
	}*/
	for (auto i = 0; i < Rectangles.size(); i++)
	{
		delete Rectangles[i];
	}
	this->Rectangles.clear();
	this->Textures.clear();
	SDL_DestroyRenderer(this->getRenderer());
}
