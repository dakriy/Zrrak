#pragma once

#include <vector>
#include "Entity.h"

using namespace std;

class Game
{
protected:
	vector<Entity *> Zombies;
	int ZombieTime;
	int secondsSinceLastWalk;
	int lastwalk;
	vector<Entity *> ZombiesToBeDeleted;
public:
	Game();
	void updateGame();
	void spawn();
	void checkHealth();
	~Game();
};