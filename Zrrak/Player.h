#pragma once

#include "SDL.h"
#include "Structs.h"
#include "Weapon.h"

class Player
{
protected:
	int Health;
	int Speed;
	int Armor;
	int Mana;
	int Level;
	int CurrentXP;
	int XPToLevel;
	int BaseDamage;
	unsigned long int Score;
	PictureFrame * Info;
	void levelUp();
	bool Alive;
	int MaxHealth;
	int MaxManna;
	Weapon * weapon;
public:
	Player();
	void updateAngle();
	int getDamage();
	void setPlayerSection(int x, int y, int h, int w);
	double getAngle();
	void setHealth(int amountToChangeHealthBy);
	void resetHeath();
	int getHealth();
	void setSpeed(int speed);
	int getSpeed();
	int getMaxHealth();
	int getArmor();
	Weapon * getWeapon();
	void updateManna(int AmountToChangeMannaBy);
	void resetManna();
	void updateCurrentXP(int AmountToChangeXPBy);
	int getXPToLevel();
	int getCurrentXP();
	int getLevel();
	void updateScore(int AmountToChangeScoreBy);
	void Render();
	void Destroy();
	void setPlayerTexutre(SDL_Texture * texture);
	bool isAlive();
	~Player();
};