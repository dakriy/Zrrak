﻿#include "Item.h"
#include "Globals.h"

Item::Item(PictureFrame * info, bool walkable, int speedmodifier)
{
	this->setInfo(info);
	this->Info->Selection = new SDL_Rect;
	this->Info->Selection->x = 0;
	this->Info->Selection->y = 0;
	this->Info->Selection->h = 32;
	this->Info->Selection->w = 32;
	this->Traversable = walkable;
	this->SpeedModifier = speedmodifier;
}

SDL_Rect* Item::getPlacement()
{
	return this->Info->Position;
}

void Item::setInfo(PictureFrame* info)
{
	if (info == nullptr)
	{
		this->Info = new PictureFrame;
	} else
	{
		this->Info = info;
	}
}

void Item::setPlacement(SDL_Rect* position)
{
	this->Info->Position = position;
}

void Item::setRenderingIndex(int* index)
{
	this->Info->RenderingIndex = index;
}

int* Item::getRenderingIndex()
{
	return this->Info->RenderingIndex;
}

void Item::setTexture(SDL_Texture* texture)
{
	this->Info->Texture = texture;
}

SDL_Texture* Item::getTexture()
{
	return this->Info->Texture;
}

SDL_Rect* Item::getSelection()
{
	return this->Info->Selection;
}

bool Item::getWalkable()
{
	return this->Traversable;
}

int Item::getSpeedModifier()
{
	return this->SpeedModifier;
}

PictureFrame* Item::getInfo()
{
	return this->Info;
}

void Item::unRender()
{
	Screen->remove_without_delete(this->getInfo());
}

Item::~Item()
{
	if (this->Info != nullptr)
	{
		delete this->Info;
	}
}
