#include "Structs.h"

PictureFrame::PictureFrame(SDL_Texture* texture)
{
	this->Texture = texture;
	type = "";
	this->RenderingIndex = nullptr;
	this->angle = 0.0;
	this->Flip = SDL_FLIP_NONE;
	this->Selection = nullptr;
	this->Position = nullptr;
}

PictureFrame::~PictureFrame()
{
	SDL_DestroyTexture(this->Texture);
	if (this->Position == nullptr)
	{
		delete this->Position;
	}
	if (this->Selection == nullptr)
	{
		delete this->Selection;
	}
	if (this->RenderingIndex == nullptr)
	{
		delete this->RenderingIndex;
	}
	this->Texture = nullptr;
}
